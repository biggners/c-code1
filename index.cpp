#include <iostream>
using namespace std;

int main() {
    int number1;
    int number2;

    cout << "Here we will ask you to enter two numbers and the program will show you the sum of both the numbers that are given to you" <<endl;

    cout << "Enter first number: " <<endl;
    cin >> number1;

    cout << "Enter second number: " <<endl;
    cin >> number2;

    int sum = number1 + number2;
    cout << "The sum of given numbers is: \t";
    cout << sum;
}